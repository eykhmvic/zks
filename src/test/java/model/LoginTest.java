package model;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginTest extends DriverBase {

    @Autowired
    private SignUpPage signUpPage;

    @Test
    public void loginSuccess() {
        signUpPage.loginSuccess();
        Assert.assertEquals("First Last", driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a/span")).getText());

    }

    /**
     * Email already exists, password is wrong.
     */
    @Test
    public void wrongPassword() {
        String email = "email@email.com";
        signUpPage.goToSignUpPage();
        signUpPage.fillEmail(email);
        signUpPage.createAccount();
        signUpPage.waitForElement(By.id("create_account_error"));
        Assert.assertEquals("An account using this email address has already been registered. Please enter a valid password or request a new one.",
                signUpPage.getSignUpInfo().getText());

    }


}
