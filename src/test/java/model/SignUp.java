package model;

import org.junit.Test;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

import static model.SignUp.ErrorCodes.ADDRESS;
import static model.SignUp.ErrorCodes.CITY;
import static model.SignUp.ErrorCodes.COUNTRY;
import static model.SignUp.ErrorCodes.COUNTRY_REQUIRED;
import static model.SignUp.ErrorCodes.EMPTY_COUNTRY;
import static model.SignUp.ErrorCodes.FIRST_NAME;
import static model.SignUp.ErrorCodes.LAST_NAME;
import static model.SignUp.ErrorCodes.PASSWORD;
import static model.SignUp.ErrorCodes.PHONE_NUMBER;
import static model.SignUp.ErrorCodes.POSTCODE;
import static model.SignUp.ErrorCodes.STATE;

public class SignUp extends DriverBase {
    @Autowired
    private Account account;
    @Autowired
    SignUpPage signUpPage;

    private void signUp() {
        String email = Utils.getRandomString(10) + "@email.com";
        signUpPage.goToSignUpPage();
        signUpPage.fillEmail(email);
        signUpPage.createAccount();
        signUpPage.waitForElement(By.xpath("//*[@id=\"center_column\"]/h1"));
        Assert.assertEquals("AUTHENTICATION", signUpPage.getAuthenticationElement().getText().toUpperCase());
        signUpPage.waitForElement(By.id("account-creation_form"));

    }


    @Test
    public void invalidEmail() {
        signUpPage.goToSignUpPage();
        signUpPage.fillEmail("wrongFormatOfEmail");
        signUpPage.createAccount();
        account.waitForElement(By.id("create_account_error"));
        Assert.assertEquals("Invalid email address.",
                signUpPage.getSignUpInfo().getText());

    }

    @Test
    public void testCreateAccountSuccessful() {
        signUp();
        fillNecessaryFieldsValid();
        account.clickSubmit();
        account.waitForElement(By.xpath("//*[@id=\"center_column\"]/h1"));
        Assert.assertEquals("MY ACCOUNT", account.getMyAccountElement().getText());
    }

    enum ErrorCodes {
        PHONE_NUMBER("You must register at least one phone number."),
        LAST_NAME("lastname is required."),
        FIRST_NAME("firstname is required."),
        PASSWORD("passwd is required."),
        ADDRESS("address1 is required."),
        CITY("city is required."),
        POSTCODE("The Zip/Postal code you've entered is invalid. It must follow this format: 00000"),
        STATE("This country requires you to choose a State."),
        COUNTRY("Country is invalid"),
        EMPTY_COUNTRY("Country cannot be loaded with address->id_country"),
        COUNTRY_REQUIRED("id_country is required.");


        private String errorMessage;

        ErrorCodes(String errorMessage) {
            this.errorMessage = errorMessage;
        }
    }

    @Test
    public void testRequiredFields() {
        signUp();
        account.setCountryByVizibleText("-");
        account.clickSubmit();
        account.waitForElement(By.xpath("//*[@id=\"center_column\"]/div"));
        List<String> listOfErrors = account.getErrorInfoElement();
        listOfErrors.sort(String::compareTo);
        List<String> listOfExpected = Arrays.asList(
                EMPTY_COUNTRY.errorMessage
                , COUNTRY.errorMessage
                , PHONE_NUMBER.errorMessage
                , ADDRESS.errorMessage
                , CITY.errorMessage
                , FIRST_NAME.errorMessage
                , COUNTRY_REQUIRED.errorMessage
                , LAST_NAME.errorMessage
                , PASSWORD.errorMessage
        );
        listOfExpected.sort(String::compareTo);
        Assert.assertEquals(listOfErrors, listOfExpected);
    }

    @Test
    public void testRequiredState() {
        signUp();
        account.setCountryByVizibleText("United States");
        account.clickSubmit();
        account.waitForElement(By.xpath("//*[@id=\"center_column\"]/div"));

        List<String> listOfErrors = account.getErrorInfoElement();
        listOfErrors.sort(String::compareTo);
        List<String> listOfExpected = Arrays.asList(
                PHONE_NUMBER.errorMessage
                , LAST_NAME.errorMessage
                , PASSWORD.errorMessage
                , ADDRESS.errorMessage
                , CITY.errorMessage
                , POSTCODE.errorMessage
                , STATE.errorMessage
                , FIRST_NAME.errorMessage

        );
        listOfExpected.sort(String::compareTo);
        Assert.assertEquals(listOfErrors, listOfExpected);
    }

    @Test
    public void testPostCodeInvalid() {
        signUp();
        fillNecessaryFieldsValid();
        account.getPostCodeInput().clear();
        account.fillPostcode("zxcvb");
        account.clickSubmit();
        account.waitForElement(By.xpath("//*[@id=\"center_column\"]/div"));
        List<String> listOfErrors = account.getErrorInfoElement();
        Assert.assertTrue(listOfErrors.contains(POSTCODE.errorMessage));
    }

    public void fillNecessaryFieldsValid() {
        account.fillFirstNameInput("TestFirstName");
        account.fillLastNameInput("TestLastName");
        account.fillPassword("TestPassword");
        account.fillAddress("TestAddress");
        account.fillCity("TestCity");
        account.waitForOptionElement(By.id("id_state"));
        account.setStateByIndex(5);
        account.fillPostcode("12345");
        account.setCountryByVizibleText("United States");
        account.fillHomePhoneNumber("123456789");
        account.fillAddressAlias("addrAlias");
    }

    public void fillUnnecessaryFields() {
        account.setTitle(Account.Title.Mr);
        account.setDay("31");
        account.setMonth("1");
        account.setYear("1994");
        account.fillFirstNameAddress("TestAddressFirstName");
        account.fillLastNameAddress("TestAddressName");
        account.fillPhoneNumber("123456789");
    }
}
