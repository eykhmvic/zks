package model;

import config.SystemConfig;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SystemConfig.class)
public class DriverBase {
    @Autowired
    WebDriver driver;

    @Test
    public void testSimple(){

    }

    void pleaseWait(int secs) {
        try {
            Thread.sleep(secs * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void clearCookie() {
        driver.manage().deleteAllCookies();
    }

}
