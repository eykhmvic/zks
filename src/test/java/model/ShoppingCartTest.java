package model;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ShoppingCartTest extends DriverBase {
    @Autowired
    SignUpPage signUpPage;
    @Autowired
    WelcomePage welcomePage;

    @Test
    public void testAddOneItem() {
        signUpPage.loginSuccess();
        welcomePage.addFirstDressToCart();

        Assert.assertEquals("1", welcomePage.getSummaryQty().getAttribute("value"));
    }

    @Test
    public void testAddTwoItems() {
        signUpPage.loginSuccess();
        welcomePage.addFirstDressToCart();
        welcomePage.addFirstDressToCart();
        Assert.assertEquals("2", welcomePage.getSummaryQty().getAttribute("value"));
    }

}
