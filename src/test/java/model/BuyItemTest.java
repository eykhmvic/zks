package model;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;

public class BuyItemTest extends DriverBase {
    @Autowired
    private Wizard wizard;
    @Autowired
    private SignUpPage signUpPage;
    @Autowired
    private WelcomePage welcomePage;

    @Test
    public void testWizard() {
        signUpPage.loginSuccess();
        welcomePage.addFirstDressToCart();
        wizard.clickProceedToCheckOut();
        wizard.clickProcessAdress();
        wizard.clickAgreeWithTerms();
        wizard.clickProcessShipping();
        wizard.clickPayByBankWire();
        Assert.assertEquals("BANK-WIRE PAYMENT.", wizard.getStatusTitle());
    }
}
