package model;

import org.apache.commons.lang.RandomStringUtils;

public class Utils {
    public static String getRandomString(int length) {
        return RandomStringUtils.random(length, true, true);
    }
}
