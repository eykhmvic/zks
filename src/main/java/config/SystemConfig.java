package config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;
import java.nio.file.Paths;


@Configuration
@ComponentScan("model")
public class SystemConfig {
    @Bean
    public WebDriver driver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        Path path = Paths.get("src/test/resources/chromedriver.exe").toAbsolutePath();
        System.setProperty("webdriver.chrome.driver", path.toAbsolutePath().toString());
        return new ChromeDriver(options);
    }

    @Bean
    public WebDriverWait webDriverWait() {
        return new WebDriverWait(driver(), 10);
    }
}
