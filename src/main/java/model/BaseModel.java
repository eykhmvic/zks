package model;

import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class BaseModel {

    @Autowired
    WebDriver driver;

    @Autowired
    public WebDriverWait webDriverWait;

    void waitForElement(final By by) {
        webDriverWait.until((ExpectedCondition<Boolean>) d -> d.findElement(by).isDisplayed());
    }

    void waitForOptionElement(final By by) {
        webDriverWait.until((ExpectedCondition<Boolean>) input -> new Select(input.findElement(by)).getOptions().size() > 1);
    }


}

