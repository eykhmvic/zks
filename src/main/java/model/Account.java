package model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Account extends BaseModel {

    enum Title {
        Mr, Mrs
    }

    private Title title;

    public void setTitle(Title title) {
        if (title == Title.Mr) {
            driver.findElement(By.id("id_gender1")).click();
        } else {
            driver.findElement(By.id("id_gender2")).click();

        }
    }

    public WebElement getFirstNameInput() {
        return driver.findElement(By.id("customer_firstname"));
    }

    public void fillFirstNameInput(String firstName) {
        getFirstNameInput().sendKeys(firstName);
    }


    public WebElement getLastNameInput() {
        return driver.findElement(By.id("customer_lastname"));
    }

    public void fillLastNameInput(String lastName) {
        getLastNameInput().sendKeys(lastName);
    }

    public WebElement getEmailInput() {
        return driver.findElement(By.id("email"));
    }

    public WebElement getPasswordInput() {
        return driver.findElement(By.id("passwd"));
    }

    public void fillPassword(String pass) {
        getPasswordInput().sendKeys(pass);
    }

    public WebElement getDayInput() {
        return driver.findElement(By.id("days"));
    }

    public void setDay(String day) {
        Select days = new Select(getDayInput());
        days.selectByValue(day);
    }

    public WebElement getMonthInput() {
        return driver.findElement(By.id("months"));
    }

    public void setMonth(String month) {
        Select months = new Select(getMonthInput());
        months.selectByValue(month);
    }

    public WebElement getYearInput() {
        return driver.findElement(By.id("years"));
    }

    public void setYear(String year) {
        Select years = new Select(getYearInput());
        years.selectByValue(year);
    }

    public WebElement getAddressInput() {
        return driver.findElement(By.id("address1"));
    }

    public void fillAddress(String address) {
        getAddressInput().sendKeys(address);
    }

    public WebElement getCityInput() {
        return driver.findElement(By.id("city"));
    }

    public void fillCity(String city) {
        getCityInput().sendKeys(city);
    }


    public WebElement getStateInput() {
        return driver.findElement(By.id("id_state"));
    }

    public void setStateByValue(String state) {
        Select years = new Select(getStateInput());
        years.selectByValue(state);
    }

    public void setStateByIndex(int index) {
        Select years = new Select(getStateInput());
        years.selectByIndex(index);
    }

    public WebElement getCountryInput() {
        return driver.findElement(By.id("id_country"));
    }


    public void setCountryByVizibleText(String country) {
        Select countries = new Select(getCountryInput());
        countries.selectByVisibleText(country);
    }

    public WebElement getPostCodeInput() {
        return driver.findElement(By.id("postcode"));
    }

    public void fillPostcode(String postcode) {
        getPostCodeInput().sendKeys(postcode);
    }

    public WebElement getFirstNameAddressInput() {
        return driver.findElement(By.id("firstname"));
    }

    public void fillFirstNameAddress(String firstname) {
        getFirstNameAddressInput().sendKeys(firstname);
    }


    public WebElement getLastNameAddressInput() {
        return driver.findElement(By.id("lastname"));
    }

    public void fillLastNameAddress(String lastname) {
        getLastNameAddressInput().sendKeys(lastname);
    }

    public WebElement getHomePhoneInput() {
        return driver.findElement(By.id("phone"));
    }

    public void fillHomePhoneNumber(String number) {
        getHomePhoneInput().sendKeys(number);
    }

    public WebElement getPhoneInput() {
        return driver.findElement(By.id("phone_mobile"));
    }

    public void fillPhoneNumber(String number) {
        getPhoneInput().sendKeys(number);
    }

    public WebElement getAddressAlias() {
        return driver.findElement(By.id("alias"));
    }

    public void fillAddressAlias(String alias) {
        getAddressAlias().sendKeys(alias);
    }

    public void clickSubmit() {
        driver.findElement(By.id("submitAccount")).click();

    }

    public List<String> getErrorInfoElement() {
        return driver.findElements(By.xpath("//*[@id=\"center_column\"]/div/ol/li"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public WebElement getMyAccountElement() {
        return driver.findElement(By.xpath("//*[@id=\"center_column\"]/h1"));
    }

}
