package model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

@Component
public class Wizard extends BaseModel {


    public WebElement getProceedToCheckOut() {
        return driver.findElement(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]"));
    }

    public void clickProceedToCheckOut() {
        getProceedToCheckOut().click();
    }

    public WebElement getProcessAdress() {
        return driver.findElement(By.xpath("//*[@id=\"center_column\"]/form/p/button"));
    }

    public void clickProcessAdress() {
        getProcessAdress().click();
    }

    public void clickAgreeWithTerms() {
        driver.findElement(By.id("cgv")).click();
    }

    public WebElement getProcessShipping() {
        return driver.findElement(By.xpath("//*[@id=\"form\"]/p/button"));
    }

    public void clickProcessShipping() {
        getProcessShipping().click();
    }

    public WebElement getPayByBankWire() {
        return driver.findElement(By.id("HOOK_PAYMENT")).findElement(By.tagName("a"));
    }

    public void clickPayByBankWire() {
        getPayByBankWire().click();
    }

    public String getStatusTitle(){
        return driver.findElement(By.id("center_column")).findElement(By.tagName("h3")).getText();
    }
}


