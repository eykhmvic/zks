package model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.stereotype.Component;

@Component
public class WelcomePage extends BaseModel {

    public WebElement getDressesElement() {
        return driver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/a"));
    }

    public void clickDresses() {
        getDressesElement().click();
    }

    public WebElement getAddToCart() {
        return driver.findElement(By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a"));
    }

    public WebElement getFirstDress() {
        return driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[1]"));
    }

    public void clickAddToCart() {
        Actions actions = new Actions(driver);
        actions.moveToElement(getFirstDress());
        actions.build().perform();
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/div[2]/a[1]")).click();
    }

    public WebElement getCart() {
        return driver.findElement(By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a"));
    }

    public void clickCart() {
        getCart().click();
    }

    public By getProceedBy() {
        return By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a");
    }

    public WebElement getProceedElement() {
        return driver.findElement(getProceedBy());
    }

    public WebElement getSummaryQty() {
        return driver.findElement(By.xpath("//*[@id=\"product_3_13_0_141889\"]/td[5]/input[2]"));
    }

    public void clickProceed() {
        getProceedElement().click();
    }

    public void addFirstDressToCart() {
        clickDresses();
        clickAddToCart();
        waitForElement(getProceedBy());
        clickProceed();
    }
    
    


}
