package model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

@Component
public class SignUpPage extends BaseModel {

    public void goToSignUpPage() {
        driver.get("http://automationpractice.com/");
        driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).click();
    }


    //create new account
    public WebElement getEmailInput() {
        return driver.findElement(By.id("email_create"));
    }

    public void fillEmail(String email) {
        getEmailInput().sendKeys(email);
    }

    public void createAccount() {
        driver.findElement(By.id("SubmitCreate")).click();
    }

    public WebElement getSignUpInfo() {
        return driver.findElement(By.xpath("//*[@id=\"create_account_error\"]/ol/li"));
    }

    public WebElement getAuthenticationElement() {
        return driver.findElement(By.xpath("//*[@id=\"center_column\"]/h1"));
    }


    //Already registered
    public WebElement getExistingEmailElement() {
        return driver.findElement(By.id("email"));
    }

    public void fillExistingEmail(String info) {
        getExistingEmailElement().sendKeys(info);
    }

    public WebElement getExistingPasswordElement() {
        return driver.findElement(By.id("passwd"));
    }

    public void fillExistingPassword(String password) {
        getExistingPasswordElement().sendKeys(password);
    }

    public void clickSignInButton() {
        driver.findElement(By.id("SubmitLogin")).click();
    }

    public void loginSuccess() {
        goToSignUpPage();
        fillExistingEmail("my@mail.com");
        fillExistingPassword("password");
        clickSignInButton();
        waitForElement(By.id("center_column"));
    }
}
